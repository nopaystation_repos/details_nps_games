require './GameDetail.rb'
require 'json'

GameDetail.all.each do |game|
  file_path = "#{game.consoleType}/#{game.region}/json/#{game.contentId}.json"
  if File.exists? (file_path)
    next
  end

  dirname = File.dirname(file_path)
  unless File.directory?(dirname)
    FileUtils.mkdir_p(dirname)
  end

  File.write(file_path, game.to_json)
end
